# 前準備

## プロジェクトを自分用にforkしよう

https://gitlab.com/tis-tiw/chouseichan/forks/new


Reactのハンズオン自体は、事前準備でcloneしていただいたリポジトリで作業できますが、
GitLab CI/CDを自分専用に設定できるよう、プロジェクトをforkします。


## 元々cloneしていたローカルリポジトリのリモートURLを変更しよう


### 現在の設定を確認

``` bash
$ git remote -v
origin  https://gitlab.com/tis-tiw/chouseichan (fetch)
origin  https://gitlab.com/tis-tiw/chouseichan (push)
```

### リモートURLを変更


(例)

``` bash
$ git remote set-url origin https://gitlab.com/sambatriste/chouseichan.git
```

### 変更されたことを確認

``` bash
$ git remote -v
origin  https://gitlab.com/sambatriste/chouseichan.git (fetch)
origin  https://gitlab.com/sambatriste/chouseichan.git (push)
```

### git pull

git pullして、新しいリモートから最新のコミットを取得しましょう。

### データベースのクリーンアップ

データベースを初期状態に戻します。

```bash
./mvnw flyway:clean
```

### 動作確認

[インストール後の動作確認](../env/post-install-check.md)と同じように動作確認をしましょう。
動作確認できたら前準備は完了です。


## ハンズオンをはじめましょう！

[イベントを登録する](./01_イベントを登録する.md)に進んでください。

わからなくなったら、[developブランチ](https://gitlab.com/tis-tiw/chouseichan/tree/develop)のコードを見てみましょう。  
コピペしてもOKです！（最終的にそのコードが理解できればOKです！）